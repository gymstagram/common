export interface IMuscle {
  _id?: string;
  name: string;
  image: string;
  description: string;
}
