export * from "./user.interface";
export * from "./exercise.interface";
export * from "./muscle.interface";
export * from "./post.interface";
export * from "./muscle-group.interface";
export * from "./training.interface";
