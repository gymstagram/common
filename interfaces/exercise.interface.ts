import { Difficulty } from "../enums";
import { IMuscleGroup } from "./muscle-group.interface";
import { IMuscle } from "./muscle.interface";

export interface IExercise {
  _id?: string;
  name: string;
  video: string;
  muscleGroup: IMuscleGroup;
  muscles: { primary: IMuscle[]; secondary: IMuscle[] };
  image: string;
  instructions: string[];
  difficultyLevel: Difficulty;
}
