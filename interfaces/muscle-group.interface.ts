import { IMuscle } from "./muscle.interface";

export interface IMuscleGroup {
  _id?: string;
  name: string;
  description: string;
  muscles: IMuscle[];
  image: string;
}
