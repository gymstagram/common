import { Difficulty } from "../enums";
import { IExercise } from "./exercise.interface";
import { IMuscleGroup } from "./muscle-group.interface";

export interface ITraining {
  _id?: string;
  name: string;
  musclesGroups: IMuscleGroup[];
  exercises: IExercise[];
  video: string;
  createdAt: Date;
  numOfLikes: number;
  likedBy: string[];
  difficultyLevel: Difficulty;
  duration: number;
}
