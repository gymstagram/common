import { ITraining } from "./training.interface";

export interface IPost {
  _id?: string;
  publisher: string;
  trainingID: string | ITraining;
  content: string;
  likedBy: string[];
  numOfLikes: number;
  date: Date;
}
