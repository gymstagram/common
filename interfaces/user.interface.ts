import { Day } from "../enums/day.enum";
import { IExercise } from "./exercise.interface";

export interface IDailyTraining {
  day: Day;
  exercises: IUserExercise[];
}

export interface IUserExercise {
  restTime: number;
  sets: number;
  reps: number;
  exercise: IExercise;
}

export interface IUser {
  _id?: string;
  username: string;
  password: string;
  isAdmin: boolean;
  trainings: IDailyTraining[];
}
