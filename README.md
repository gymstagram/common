1. Open Terminal
2. Run command: "npm login"
3. Log to npm with credentials that are in the whatsapp final project description
4. npm run build (changes won't be applied if this is skipped)
5. Change the minor version in package.json
6. npm publish
7. Notify everyone about the new version
